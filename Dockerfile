FROM python:3.6.2-alpine

ENV REPO=https://rudolfasd@bitbucket.org/rudolfasd/xsite.git
ENV TARGET_DIR=/srv/django
ENV HTTP_PORT=8000
ENV UWSGI_PORT=8001

RUN apk update && apk upgrade && \
  apk add --no-cache bash git

RUN apk add --no-cache build-base && \
  apk add --no-cache linux-headers && \
  apk add --no-cache pcre-dev

RUN pip install uwsgi -I --no-cache-dir

WORKDIR ${TARGET_DIR}

RUN git clone ${REPO} ${TARGET_DIR}

RUN pip install -r requirements.txt && \
  rm -rf ~/.cache/pip

CMD exec uwsgi --module xsite.wsgi \
			  --master \
			  --http=0.0.0.0:${HTTP_PORT} \
			  --socket=0.0.0.0:${UWSGI_PORT} \
			  --enable-threads \
			  --max-requests=5000 \
			  --vacuum

EXPOSE ${HTTP_PORT} ${UWSGI_PORT}